# coding=utf-8
# -*- coding:utf-8 -*-
import locale

import sys
import xlrd
import gridfs
import xlwt
import xlsxwriter
import datetime
import pandas as pd
from bson import ObjectId
from django.core.serializers import json
from pprint import pprint
from pymongo import MongoReplicaSetClient, ReadPreference, MongoClient
from xlrd import open_workbook


locale._setlocale(locale.LC_ALL)

reload(sys)
sys.setdefaultencoding('utf-8')

def insert(order_id,ship_date,ship_mode,customer_id,customer_name,segment,city,categorie,sub_categorie,product_name,sale):
	client = MongoClient('localhost',27017)
	collection = client.excel_mig.Contents

	add_item = {
		"order_id": order_id,
		"ship_date": ship_date,
		"ship_mode": ship_mode,
		"customer_id": customer_id,
		"customer_name": customer_name,
		"created_date": datetime.datetime.utcnow(),
		"segment": segment,
		"city": city,
		"categorie": categorie,
		"sub_categorie": sub_categorie,
		"product_name": product_name,
		"sale": sale
	}

	try:
		item_id = collection.insert_one(add_item)
		print(item_id)

	except Exception as e:
		pprint(e)		



def app():
	xl_workbook = pd.ExcelFile('Sample-Superstore.xls')
	df = xl_workbook.parse('Orders')

	order_ids = df['Order-ID'].tolist()
	ship_dates = df['Ship-Date'].tolist()
	ship_modes = df['Ship-Mode'].tolist()
	customer_ids = df['Customer-ID'].tolist()
	customer_names = df['Customer-Name'].tolist()
	segments = df['Segment'].tolist()
	citys = df['City'].tolist()
	categories = df['Category'].tolist()
	sub_categories = df['Sub-Category'].tolist()
	product_names = df['Product-Name'].tolist()
	sales = df['Sales'].tolist()

	for i, item in enumerate(order_ids):
		insert(order_ids[i],ship_dates[i],ship_modes[i],customer_ids[i],customer_names[i],segments[i],citys[i],categories[i],sub_categories[i],product_names[i],sales[i])


app()