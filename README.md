# Python 2.7, Pip, Virtualenv
git clone https://{{username}}@bitbucket.org/kadirusen/excel-migration-mongodb.git cd excel-migration-mongodb virtualenv venv

# Create Virtualenvironment & install depedencies
$ source venv/bin/activate $ pip freeze | xargs pip uninstall -y $ pip install -r requirements.txt

# Who do I talk to?
$ python app.py